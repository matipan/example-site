# Example site for Hugo OpenFaaS template
Example site showing how to deploy a Hugo site as an OpenFaaS function with custom domain names and free TLS certificates.

Check out the [blog post](https://blog.matiaspan.dev/posts/bringing-serverless-to-a-webpage-near-you-with-hugo-and-kubernetes/). 

This site lives at https://my-site.matiaspan.dev
